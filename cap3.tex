\chapter{El suicidio del pensamiento}%
\label{cha:El suicidio del pensamiento}

\textsc{las frases callejeras} no solamente son vigorosas, sino también sutiles: porque una figura de
lenguaje con frecuencia puede llegar a ser demasiado simple para prestarse a definición. Frases como
\say{fuera de uso}, o \say{fuera de tono}, podrían haber sido acuñadas por el señor Henry James,\footnote{Henry James (1843\enraya1916) fue un escritor británico considerado como uno de los mejores novelistas de la literatura inglesa} en una agonía
de precisión verbal. Y no hay verdad tan sutil como aquella de la cotidiana frase sobre el hombre que
tiene el corazón bien puesto. Abarca una idea de proporción normal; no sólo existe una cierta función,
sino que también esa función está correctamente relacionada con otras funciones. Por cierto que lo
opuesto de esa frase, describiría con gran exactitud, la piedad y la ternura, en cierta forma morbos, de los
modernos más representativas. Si por ejemplo tuviera que describir con sinceridad el carácter del señor
Bernard Shaw, no podría expresarme más exactamente que diciendo que, posee un corazón generoso y
heroicamente amplio, pero no es un corazón bien puesto. Y eso mismo ocurre con la sociedad típica de
nuestro tiempo.

El mundo moderno no es malo; en cierto modo el mundo moderno es demasiado bueno. Está lleno
de feroces y malgastadas virtudes. Cuando se perjudica una empresa religiosa (como se perjudicó el
Cristianismo con la Reforma) no es solamente de confusión espléndida; es algo brillante y sin forma, al
mismo tiempo llamarada y mancha. Pero el círculo de la luna es tan claro e inconfundible, tan periódico e
inevitable como el círculo de Euclides sobre un pizarrón. Porque la luna es completamente razonable; es
la madre de los lunáticos, y a todos ellos les ha dado su nombre.

A causa de los vicios desencadenados. Los vicios, por cierto se desencadenan y se extienden y
causan perjuicios. Pero las virtudes también andan desencadenadas; y las virtudes se extienden más
desenfrenadas y causan perjuicios más terribles. El mundo moderno está lleno de viejas virtudes cristianas
que se volvieron locas. Enloquecieron las virtudes porque fueron aisladas unas de otras y vagan por el
mundo solitarias.

De ahí que algunos cientistas se preocupan por la verdad; y su verdad es despiadada, y de allí que
algunos humanistas se preocupan sólo de la piedad y su piedad (lamento decirlo) frecuentemente es
falseada. Por ejemplo: el señor Blatchford ataca al cristianismo porque está loco por una virtud cristiana;
la puramente mística y casi irracional virtud de la caridad. Tiene la extraña idea de que facilitará el perdón
de los pecados, diciendo que no hay pecados que perdonar. El señor Blatchford es no solamente uno de
los primeros cristianos; es el único de los primeros cristianos que realmente mereció ser comido por los
leones. Porque en su caso, la acusación pagana es verdadera: su misericordia significa anarquía. En
realidad, por ser tan humano, es enemigo de la raza humana. Como extremo opuesto podríamos tomar al
realista agriado, que deliberadamente mató en sí todo placer humano, con fábulas alegres o con el
endurecimiento del corazón. Torquemada, torturaba físicamente a la gente, en bien de la verdad moral.

Zola tortura a la gente moralmente, en bien de la verdad física. Pero en tiempos de Torquemada, por lo
menos existía un sistema que hasta cierto punto permitía que la rectitud y la paz se besaran. Ahora, ambas
no se saludan ni con una inclinación de cabeza.

Pero podríamos encontrar un caso mucho más concluyente que estos dos de la piedad y de la
verdad, en la sorprendente dislocación de la humanidad.

Aquí sólo nos concierne tratar de un aspecto de la humildad. Por mucho tiempo, humildad ha
significado una restricción de la arrogancia e infinitud del apetito del hombre. Del hombre que siempre
estaba aventajando a sus misericordias con el continuado invento de necesidades nuevas.

Su propia capacidad de goce destruía la mitad de sus goces. Procurándose placeres, perdió el placer
principal; porque el principal placer es la sorpresa. De ahí resulta evidente que si el hombre quiere hacer
amplio a su mundo, él debe estar siempre haciéndose pequeño. Aún las ciudades más encumbradas y los
pináculos inclinados por su propia altura, son creaciones de la humildad. Los gigantes que derriban
montes como si fueran pasto, son creaciones de la humildad. Las torres que se pierden en lo alto por
encima de la estrella más solitaria en su lejanía, son creaciones de la humildad. Porque las torres no son
altas sino cuando las miramos desde abajo; y los gigantes no son gigantes sino más grandes que nosotros.

Toda esa imaginación de lo gigantesco, que es quizá el más vigoroso de los placeres del hombre, en el
fondo es enteramente humilde. Sin humildad es imposible gozar de nada; ni aun de la soberbia.

Pero lo que nos hace padecer el presente es la modestia mal ubicada. La modestia se ha mudado del
órgano de la ambición. La modestia se ha instalado en el órgano de la convicción: la cual nunca se la
había destinado.

El hombre estaba destinado a dudar de sí; pero no de la verdad; ha sucedido precisamente lo
contrario.

Actualmente la parte del hombre que el hombre proclama, es exactamente la parte que no debía
proclamar: su propio yo. La parte que pone en duda, es exactamente la parte de la cual no debía dudar: la
razón Divina. Huxley, predicó una humildad que se conformaba con aprender de la naturaleza. Pero el
escéptico de nuevo cuño es tan humilde, que duda hasta de poder aprender. De ahí resulta que si nos
hubiéramos apresurado a decir que no existe una humildad típica de nuestro tiempo, nos hubiéramos
equivocado. La verdad es que hay una real humildad típica de nuestro tiempo. Pero ocurre que,
prácticamente, es una humildad tan envenenada como la más desorbitada de las postraciones del asceta.

La vieja humildad era una espuela que impedía al hombre detenerse; no un clavo en su zapato que le
impedía proseguir. Porque la vieja humildad hacía que el hombre dudara de su esfuerzo, lo cual lo
conducía a trabajar más duro. Pero la nueva humildad hace que el hombre dude de su meta, lo cual lo
conduce a cesar su esfuerzo por completo.

En cualquier esquina podemos encontrar un hombre pregonando la frenética y blasfema confesión
de que puede estar equivocado. Cada día nos cruzamos con alguno que dice, que, por supuesto, su teoría
puede no ser la cierta.

Por supuesto, su teoría debe ser la cierta, o de lo contrario, no sería su teoría. Estamos en camino de
producir una raza de hombres mentalmente demasiado modestos para creer en la tabla de multiplicar. Nos
hallamos en peligro de ver filósofos que duden de la ley de gravedad, por considerarla como un simple
producto de sus imaginaciones. Los farsantes de otros tiempos eran demasiado orgullosos para dejarse
convencer; pero éstos son demasiado humildes para poder ser convencidos. Los humildes heredan la
tierra; pero los escépticos modernos son demasiado humildes, hasta para reclamar su herencia. Y
precisamente esta impotencia intelectual es nuestro segundo problema.

El último capítulo se refería a un hecho observado: que el peligro de morbidez que puede correr un
hombre, proviene más de su corazón que de su imaginación. No se intentaba atacar la autoridad de la
razón; su objeto más bien fue defenderla; porque necesita defensa.

Todo el mundo moderno está en guerra con la razón; y la torre, ya vacila.

Con frecuencia se dice que los sensatos no hallan respuesta para el enigma de la religión. Pero la
dificultad con nuestros sensatos, no es que no puedan ver la respuesta, sino que no pueden ver ni siquiera
el enigma. Son como niños suficientemente estúpidos como para no notar nada paradójico en la
manifestación de que una puerta no es una puerta. Los tolerantes modernos, por ejemplo, hablan sobre la
autoridad religiosa, no solamente como si no hubiera razón alguna de su existencia, sino como si nunca
hubiera habido una razón para que exista.

A más de no ver su base filosófica, no pueden siquiera ver su causa histórica. La autoridad religiosa,
ha sido con frecuencia opresiva e irrazonable, tal como cada sistema legislativo (y especialmente el
nuestro actual) ha sido duro y culpable de una penosa apatía. Es razonable atacar a la policía; también es
glorioso. Pero los modernos críticos de la autoridad religiosa, son como hombres que atacaran a la policía,
sin nunca haber oído hablar de asaltantes. Porque existe un grande y posible peligro para la mente
humana; un peligro tan real como el de un asalto. Contra él, bien o mal, la autoridad Religiosa se irguió
como una barrera. Y contra él, algo por cierto debe erguirse como barrera si es que nuestra raza debe
salvarse de la ruina.

Ese peligro consiste en que el intelecto humano es libre de autodestruirse. Tal como una generación
podría impedir la existencia de la generación siguiente, recluyéndose toda en monasterios o arrojándose al
mar; así un núcleo de pensadores puede impedir, hasta cierto punto, los pensamientos subsiguientes,
enseñando a la nueva generación que no existe validez en ningún pensamiento humano. Sería cargoso
hablar siempre de la alternativa entre la razón o la fe. La razón en sí misma es un objeto de la fe. Es un
acto de fe afirmar que nuestro pensamiento no tiene relación alguna con la realidad.

Si usted es puramente un escéptico, tarde o temprano se hará esta pregunta: ¿Por qué todo puede
andar bien, aun la observación y la deducción? ¿Por qué la buena lógica es tan engañosa como la mala
lógica? ¿Ambas son actividades en el cerebro de un mono sorprendido?
Hay un pensamiento que detiene el pensamiento. Y ese es el único pensamiento que debería ser
detenido. Ese es el mal concluyente contra el cual se dirigió toda la autoridad religiosa. Recién aparece al
final de edades decadentes como la nuestra; y el señor H.\,G. Wells,\footnote{Herbert George Wells (1866\enraya1946) fue un escritor británico cuyas obras abarcan desde novelas hasta la crítica social y la sátira. Ateo y socialista, Wells abogó por la paz y trabajó en textos fundacionales de la liga de las naciones (el precursor fallido de la \textsc{onu}).} ya izó su estridente bandera; ha
escrito una delicada pieza de escepticismo llamada \emph{Las dudas del instrumento}. Allí interroga al cerebro
e intenta excluir la realidad, hasta de sus propias afirmaciones, pasadas, presentes y por venir. Contra esta
ruina lejana, se organizó y se jerarquizó originariamente, todo el sistema militar de la religión. Las
creencias y las cruzadas, las jerarquías y las persecuciones, no fueron organizadas según la ignorancia para suprimir la razón.

El hombre, por un instinto ciego sabía que si las cosas fueron discutidas
ensañadamente, la razón pudo ser discutida primero. La autoridad para absolver que tienen los sacerdotes;
la autoridad de los papas para determinar autoridades; aun la autoridad para aterrar de los inquisidores,
eran solamente sombrías defensas erigidas en torno de una autoridad central más indemostrable, más
sobrenatural que todas: la autoridad para pensar que tiene el hombre. Sabemos ahora que las cosas son
así; no tenemos excusa para ignorarlo. Porque a través de la vieja rueda de autoridades, podemos oír al
escepticismo crujiente, y al mismo tiempo ver a la razón íntegra y fuerte sobre su trono.

En tanto que la religión marche, la razón marcha. Porque ambas son de la misma primitiva y
autoritaria especie. Ambas son métodos que prueban y no pueden ser probados. Y en la acción de destruir
la idea de la autoridad divina, hemos destruido sobradamente la idea de esa autoridad humana, por la cual
podemos abreviar una división muy larga. Con un rudo y sostenido tiroteo, hemos querido quitar la mitra
al hombre pontificio, y junto con la mitra le arrebatamos la cabeza.

A menos que a esto se le llame divagación, tal vez fuera conveniente repasar rápidamente, los
principales modos de pensar modernos, que han causado este efecto de detener el pensamiento. Tuvieron
ese efecto el materialismo y la teoría de que todo es producto de una ilusión individual; porque si la mente
es mecánica, el pensar no puede ser muy divertido; y si el cosmos no es real, no hay nada en qué pensar.

Pero en estos casos el efecto es indirecto y dudoso. En algunos casos es directo y evidente; especialmente
en el caso de lo que por lo general se llama evolucionismo.

El evolucionismo es un buen ejemplo de esta inteligencia moderna, que si algo destruye, se destruye
a sí misma. El evolucionismo es, o una ingenua explicación científica de cómo sucedieron algunos
fenómenos terráqueos, o si es algo más que eso, es un ataque al pensamiento mismo. Si el evolucionismo
destruye algo, no destruye a la religión sino al racionalismo. Si la evolución significa simplemente que
algo positivo llamado mono, se convirtió, muy lentamente, en algo positivo llamado hombre, entonces es
inofensivo hasta para el más ortodoxa, porque un Dios personal, puede hacer las cosas, tanto lenta como
rápidamente, en especial si como el Dios Cristiano, está situado fuera del tiempo.

Pero si evolución quiere decir algo más, significa que no existe cosa tal como un mono a convertir,
ni cosa tal como un hombre en el cual ser convertido. Significa que no existe tal cosa como una cosa. A lo
más existe una sola cosa; y esa, es el flujo del todo y de la nada. Esto, es un ataque no contra la fe, sino
contra la mente; no es posible pensar si no hay riada en qué pensar. No es posible pensar sin que el
pensamiento esté separado de su objeto. Descartes dijo: \textquote{Yo pienso; por consiguiente existo}.\footnote{\say{Cogito, ergo sum}. Esta proposición filosófica apareció por primera vez en los \say{Discursos Sobre el Método} de Descartes.} El filósofo
evolucionista invierte y hace negativo el epigrama. Dice: \textquote{Yo no existo; por consiguiente no puedo
pensar}.

Luego está el opuesto ataque al pensamiento, aquel anticipado por el señor H.\,G. Wells cuando
insiste en que cada cosa aislada es \say{única}, y en que no existen categorías. También esta teoría es
puramente destructiva.

Pensar significa relacionar cosas y detenerse cuando no pueden ser relacionadas. Es obvio decir,
que este escepticismo prohibitivo del pensamiento, prohíbe también el lenguaje: un hombre no puede
abrir la boca sin contradecirlo. De ahí que cuando el señor H.\,G. Wells dice (como lo hizo en alguna
parte) \textquote{todas las sillas son completamente diferentes}, expresa no solamente un error, sino también una
contradicción de términos. Si todas las sillas son por completo diferentes, no se las puede llamar \say{todas
las sillas}.

Semejante a esta es la teoría progresista que sostiene que alteramos la prueba en vez de tratar de
pasarla. Con frecuencia oímos decir, por ejemplo, \textquote{lo que es bien en una época es mal en otra}. Esto es
muy razonable si significa que existe una meta permanente, y que ciertos sistemas logran alcanzarla en
ciertos y determinados tiempos y no en otros. Digamos: si las mujeres desean ser elegantes, puede que en
una época lograrán su deseo engordando y en otra época adelgazando. Pero no se podría decir que
alcanzan su meta dejando de desear ser elegantes y comenzando a desear ser ovaladas. Si el tipo varía
¿cómo podría haber perfeccionamiento, si éste implica la permanencia de un tipo? Vietzscke inició la
insensata idea de que los hombres una vez fueron en pos de un bien que ahora nosotros llamamos mal; si
fuera así no podríamos ni hablar de aventajarlos o aún de aparejarnos a ellos. ¿Cómo podría usted
alcanzar a Pérez siendo que usted camina en dirección opuesta? No se puede discutir si un pueblo,
procurando hacerse miserable tuvo más éxito que el éxito que tuvo otro procurando hacerse feliz. Sería
como discutir si Milton fue más puritano de lo que un cerdo es gordo.

Cierto es que un hombre (un hombre tonto) podría cambiar su ideal o su objeto. Pero en cuanto al
ideal, en si es incambiable. Si el admirador de la alteración desea controlar su propio progreso, debe ser
rigurosamente leal al ideal de la alteración; no debe ponerse a flirtear alegremente con el ideal de la
monotonía. El progreso en sí mismo no puede progresar. Vale la pena destacar de paso, que cuando
Tennyson,\footnote{Alfred Tennyson (1809\enraya1892) fue un poeta británico} en forma bastante alocada y débil, dio la bienvenida a la idea de la variación infinita de la
sociedad, instintivamente empleó una metáfora que sugiere algo de encarcelado hastío. Escribió:
\textquote{Dejad al gran mundo extenderse hacia los ruidosos abismos de la variante.}
Pensó que la variación en sí es un invariable abismo, y eso es. La variación, aproximadamente es el
abismo más árido y estrecho en que pueda colarse un hombre.

No obstante, aquí lo principal es que esta idea de una alteración fundamental del tipo, es una de las
cosas que hacen simplemente imposible, pensar en el pasado o en el futuro.

La teoría de una alteración completa en los prototipos de la historia humana, no solamente nos priva
del placer de honrar a nuestros padres; nos priva hasta del más moderno y aristocrático placer de
despreciarlos.

Este escueto sumario de las fuerzas destructivas del pensamiento contemporáneo, no estaría
completo si careciera de alguna referencia al pragmatismo;\footnote{Doctrina que toma el valor práctico de las cosas como criterio de la verdad.} porque aunque aquí lo haya empleado y lo
defienda en todas partes como guía preliminar de la verdad, se hace de él una aplicación exagerada que
implica la ausencia total de verdad alguna. Mi concepto, puede expresarse brevemente, así. Estoy de
acuerdo con el pragmatismo en que la aparente verdad objetiva no lo es todo; en que existe una legítima
necesidad de creer las cosas que son necesarias a la mente humana. Mas yo agrego que una de estas
necesidades, es precisamente la de creer en la verdad objetiva. El pragmático aconseja al hombre creer lo
que se debe creer y no preocuparse de lo Absoluto. Pero precisamente una de las cosas que debe creer es
lo Absoluto. 

Por cierto, esta filosofía es una especie de paradoja verbal. El pragmatismo es una cuestión de
necesidades humanas y una de las primeras necesidades humanas, es ser algo más que un pragmático. El
pragmatismo extremoso es tan inhumano como el determinismo al cual vigorosamente ataca. El
determinista (que para hacerle justicia no tiene pretensiones de ser humano), se burla del sentido humano
para hacer la elección activa del hecho. El pragmatista, que profesa ser esencialmente humano, se burla
del sentido humano frente al hecho en acción.

Para resumir lo expuesto hasta aquí, podríamos decir que las filosofías corrientes más
características, no sólo tienen rasgos de manía, sino rasgos de manía suicida. El investigador ha dado con
la cabeza contra los límites del pensamiento humano; y se la rompió. Esto es lo que hace tan inútiles las
advertencias del ortodoxo y tan vana la jactancia de los vanguardistas sobre 14 peligrosa adolescencia del
libre pensamiento. Lo que estamos presenciando no es la adolescencia del libre pensamiento, es su vejez
decrépita y su disolución terminante. Es inútil que los obispos y los sabios discutan qué horribles sucesos
vendrán si el desenfrenado escepticismo sigue su curso.

Es en vano que los ateos elocuentes hablen de las grandes verdades que se revelarán una vez que
veamos los comienzos del libre pensamiento. Ya hemos visto su término.

No tiene ya preguntas por hacer; se ha interrogado a sí mismo. No es posible evocar visión más
salvaje que la de una ciudad cuyos hombres se preguntan si tienen persona.

No es posible imaginar un mundo más escéptico que aquél en el cual los hombres dudan de que el
mundo existe. El libre pensamiento podría haber llegado a la quiebra más rápida y limpiamente, de no
haber sido débilmente trabado por la aplicación de las indefendibles leyes de la blasfemia o por la absurda
pretensión de que Inglaterra moderna es cristiana. Pero de cualquier modo hubiera quebrado.

Los ateos militantes aún son injustamente perseguidos; pero más por ser una antigua minoría que
por ser una minoría nueva. El libre pensamiento ha agotado su propia libertad.

Está hastiado de sus propios éxitos. Si ahora algún libre pensador ansioso, saluda a la libertad
filosófica como a un amanecer, es igual al hombre de Mark Twain que envuelto en sus sábanas salió a ver
la salida del sol y llegó justo a tiempo para ver su ocaso. Si algún pastor alarmado dice todavía que sería
terrible que se extendiera la oscuridad del libre pensamiento, sólo podríamos responderle con las palabras
del señor H. Belloc: \textquote{Le ruego no se turbe previendo el incremento de fuerzas en disolución. Se ha
equivocado en las horas de la noche; ya es la mañana}. No hemos dejado preguntas por preguntar. Hemos
buscado interrogaciones en los rincones más sombríos y en las cumbres más inexploradas. Hemos hallado
todas las preguntas que se pueden hallar. Ya es hora de que cesemos de buscar interrogantes y
comencemos a buscar respuestas.

Pero hay que agregar una palabra más. Al comienzo de esta conversación negativa dije que nuestra
ruina mental la trae la razón desenfrenada y no la desenfrenada imaginación. Un hombre no se vuelve
loco por hacer una estatua de 1.600 metros de altura; pero puede volverse loco pensándola en pulgadas
cúbicas. Ahora, una escuela de pensadores comprendiéndolo así, se ha abalanzado a esa verdad, creyendo
hallar en ella la forma de remozar la salud paganizada del mundo.

Vieron que la razón destruye; pero dicen que la voluntad crea. La autoridad ulterior reside en la
voluntad, no en la razón. El punto supremo es no el por qué un hombre requiere algo, sino el hecho de
que lo requiera. No tengo espacio para describir o exponer esta filosofía de la Voluntad.

Supongo que llegó a través de Nietzsche, que predicó algo llamado egoísmo. Por cierto Nietzsche
fue bastante ingenuo, porque renegó del egoísmo simplemente predicándolo. Puesto que predicar algo, es
renunciar a ello. Primero, el egoísmo llama guerra despiadada a la vida y luego se toma todas las
molestias posibles para arrojar sus enemigos a la guerra. Predicar el egoísmo es practicar el altruismo.

Pero como quiera que empiece, su punto de vista es bastante común en la literatura corriente. La principal
disculpa de estos pensadores, es que no son pensadores: son actores. Dicen que elegir es en sí divino. De
ahí que el señor Bernard Shaw haya atacado la vieja idea según la cual los actos deben juzgarse conforme
al típico deseo de felicidad. Dice que los actos del hombre no son causados por su tendencia a la felicidad,
sino por un esfuerzo de voluntad.

No dice: \textquote{el jamón me hará feliz}, sino \textquote{yo quiero jamón}. Y en esta temía, otros le siguen aun con
mayor entusiasmo. El señor John Davidson, un destacado poeta, tanto se apasiona por este asunto, que se
ve obligado a escribir en prosa. Publicó sobre él, una obra breve con varios extensos prefacios. Lo cual es
bastante natural para el señor Bernard Shaw, cuyas obras son todas prefacios. El señor Shaw (sospecho)
es el único hombre sobre la tierra que haya escrito poesía. Pero ese señor Davidson, que puede escribir
poesía excelente y en vez de escribirla debe escribir complicada metafísica en defensa de esta doctrina de
la voluntad, demuestra que la doctrina de la voluntad, se ha posesionado de los hombres. Aún el señor H.\,G. Wells,
a medias ha hablado en su lenguaje diciendo que el hombre debe probar sus actos no como un
pensador sino como un artista; diciendo \textquote{siento que esta curva está bien} o \textquote{esta línea debe ir en tal
forma.} Todos están agitados; y bien pueden estarlo. Porque por esta doctrina de la divina autoridad de la
voluntad, creen que pueden liberarse de la fortaleza carcelaria del racionalismo. Creen que pueden
escapar. Pero no pueden.

Esta alabanza confusa a la volición, concluye en la misma destrucción confusa que la observancia
de la lógica. Así como el absoluto libre pensamiento, implica dudar del pensamiento en sí, exactamente
así, la aceptación exclusiva del \say{querer}, paraliza la voluntad.

El señor Bernard Shaw, no ha percibido la positiva diferencia que existe entre el viejo experimento
utilitario del placer (que por supuesto es burdo y mal expresado) y lo que él sostiene. La real diferencia
entre la experimentación de la felicidad y la experimentación de la voluntad consiste en que la
experimentación de la felicidad es un experimento y la otra, no Io es. Se puede discutir si el acto de un
hombre que se precipita desde una colina, tiende a la felicidad; no se puede discutir que ese acto derive de
la voluntad. Por supuesto que deriva. Se puede alabar un acto diciendo que se le había destinado a
procurar placer o dolor, a descubrir la verdad o salvar el alma. Pero no se le puede alabar porque implique
voluntad, porque tal alabanza, no es más que decir que es un acto. Según esta alabanza de la voluntad, no
se puede elegir un camino mejor que otro. Y sin embargo, la elección de un camino por ser mejor que
otro, es la verdadera definición de la voluntad que se está alabando.

La adoración de la voluntad, es la negación de la voluntad. Admirar exclusivamente la elección en
sí, es rehusarse a elegir. Si el señor Bernard Shaw, viene a mí y me dice: \textquote{quiera algo}, es como si me
dijera: \textquote{no me importa lo que usted quiera}, que es como decir: \textquote{yo no tengo voluntad en general, porque
la esencia de la voluntad es ser particular}. Un brillante anarquista como el señor John Davidson, se irrita
contra la moralidad ordinaria y de ahí invoca a la voluntad, la voluntad para cualquier cosa. Lo único que
quiere es que la humanidad quiera algo. Pero la humanidad quiere algo. Quiere la moralidad ordinaria. Se
rebela contra la ley y nos dice que queramos algo o cualquier cosa. Pero algo hemos querido. Hemos
querido la ley contra la cual se rebela. Todos los fanáticos de la voluntad, desde Nietzsche hasta el señor
Davidson, están realmente privados de volición. No pueden \say{querer}; apenas pueden desear. Y si alguien
exige. una prueba, se la puede hallar fácilmente en este hecho: que siempre hablan de la voluntad como
de algo que puede dilatarse y quebrarse. Pero ocurre exactamente lo opuesto. Cada acto de voluntad, es
un acto de autolimitación. Desear acción, es desear limitación. En este sentido, cada acto voluntario, es un
acto de autosacrificio.

Cuando se elige algo, se rechaza todo lo demás.

Esta objeción que los hombres de la dicha escuela, aplicaban al acto de contraer matrimonio, es en
realidad la objeción adecuada para cada acto. Cada acto es una selección y una exclusión irrevocable. Tal
como cuando usted se casa con una mujer renuncia a todas las demás, así cuando elige un camino de
acción, reunía a todos los otros caminos. Si usted acepta ser Rey de Inglaterra, renuncia al puesto de
Ujier de Brompton. Si usted se va a Roma, inmola una rica y placentera vida en Wimbledon. La
existencia de este lado negativo o limitante de la voluntad, hace poco más que jocosa la charla de los
anárquicos, adoradores de la voluntad. Por ejemplo; el señor John Davidson nos dice que nada podremos
hacer contra el \say{Vos, no podréis}; pero seguramente \say{Vos, no podréis}, sólo es uno de los corolarios
imprescindibles del \say{yo quiero}. \say{Yo quiero ir a la Reyista del Lord Mayor, y Vos, no podréis
detenerme}. El anarquismo nos conjura a ser artistas creadores y audaces, sin importársele de las leyes o
de los límites. El arte es limitación; la esencia de cada pintura está en sus perfiles. Si usted dibuja una
jirafa, debe dibujarla con el pescuezo largo.

Si en su audaz forma creadora, se conserva libre de dibujar una jirafa. con el pescuezo corto,
ciertamente descubrirá que usted no es libre de dibujar una jirafa. En el momento de entrar al mundo de
los hechos, se entra al mundo de las limitaciones.

Usted puede liberar las cosas de sus leyes accidentales o accesorias, pero no de las leyes propias de
sus naturalezas. Si usted quiere, puede liberar a un tigre de sus rejas, mas no lo libre de su cautiverio. No
libre al camello de su joroba: puede estar librándolo de ser camello. No se pasee como un demagogo
incitando a los triángulos a evadirse de sus tres lados. Si un triángulo se sale de sus tres lados, su vida
llegará a un lamentable término. Alguien escribió un artículo titulado: \say{Los Amantes de los Triángulos};\footnote{Quizás Chesterton se refiere a \say{The Loves of the Triangles}, un poema publicado por un \say{Mr.\,Higgins} en el \say{Anti-Jacobin} un periódico del partido Tory}
nunca lo leí, pero estoy seguro de que si los triángulos alguna vez fueron amados, fueron amados por ser
triangulares. Este es ciertamente el caso de toda creación artística, la cual en cierto modo, es el más
acabado ejemplo de voluntad pura\ldots\ Los artistas aman sus limitaciones: constituyen lo que están haciendo.

El pintor, se alegra de que la tela sea chata. El escultor se alegra de que el yeso sea incoloro.

En caso de que el ejemplo no fuera claro; podría ilustrarse con un ejemplo histórico. La Revolución
Francesa fue algo heroico y decisivo, porque los Jacobinos quisieron algo definitivo y limitado. Quisieron
la libertad de la democracia, pero quisieron también todas las restricciones de la democracia. Desearon
tener votos y no tener títulos. Los Republicanos tuvieron su aspecto ascético en Franklin o en
Robespierre, tanto como en Danton y Wilkes tuvieron su aspecto expansivo. Por lo tanto crearon algo
sólido en sustancia y apariencia; la justa igualdad social y el bienestar campesino de Francia. Pero desde
entonces, la mente revolucionaria o especulativa de Europa, ha decaído, rechazando toda propuesta a
causa de las limitaciones de la proposición.

El liberalismo fue rebajado a liberalidad. Los hombres intentaron hacer intransitivo al verbo
transitivo: \say{revolucionar}. El jacobino podía decir no solamente contra qué sistema se rebelaría sino (lo
que es más) contra qué sistema no se hubiera rebelado; en qué sistema habría puesto su confianza. Pero el
rebelde de nuevo cuño es un escéptico y nada cree por entero. No tiene lealtad; por consiguiente no puede
ser nunca un verdadero revolucionario. Y el hecho de que duda de todo, por cierto lo fastidia cuando
quiere proclamar algo. Porque toda proclamación implica una doctrina moral determinada; y el
revolucionario no sólo duda de la institución que proclama sino también de la doctrina por la cual la ha
proclamado. De ahí resulta que escriba un libro quejándose porque opresión imperial insulta la pureza de
las mujeres y después escriba otro libro (sobre el problema sexual) en el que a su vez las insulta. Maldice
al Sultán porque las muchachas cristianas pierden la virginidad y luego maldice a la señora Grundy\footnote{Protectora de la juventud femenina.}
porque la conserva. Como político gritará que \textquote{la guerra es una pérdida de vidas} y como filósofo gritará
que \textquote{la vida es una pérdida de tiempo}. Un ruso pesimista denunciará a un policía por haber matado un
campesino y luego, por un proceso filosófico de alto vuelo, probará que el campesino merecía la muerte.

Un hombre proclama que el matrimonio es una mentira y, luego denuncia al aristócrata canalla, porque lo
trata como si fuera una mentira. A la bandera le dice juguete y luego increpa a los opresores de Polonia o
de Irlanda porque les han quitado su juguete. El hombre de esta escuela, primero va al meeting político
donde se queja de que a los salvajes se les trata como a bestias; y luego toma el sombrero y el paraguas y
se va a un meeting científico en el que prueba que os salvajes son verdaderamente bestias. Abreviando, el
revolucionario moderno, siendo infinitamente escéptico, siempre está ocupado en minar sus propias
minas.

En su libro sobre política ataca a los hombres por pisotear la moral; en su libro sobre ética ataca la
moral por humillar al hombre. Como consecuencia, el revoltoso moderno se ha vuelto completamente
inútil para toda tentativa de revuelta. Rebelándose contra todo, ha perdido su derecho a rebelarse contra
algo.

Podría agregarse que la misma laguna y la misma bancarrota se observa en todos los tipos terribles
y violentos de literatura; especialmente en la satírica.

La sátira será loca y anárquica, pero presupone la admisión de cierta superioridad de unas cosas
sobre as; presupone un modelo, típico.

Cuando en la calle los chicos se ríen de la gordura de un distinguido periodista, inconscientemente
lo comparan con el mármol de Apolo. Y la extraña desaparición de la sátira de nuestra literatura, es un
ejemplo de las cosas violentas que se desvanecen por carecer de alguna base sobre la cual ejercer
violencia. Nietzsche, tiene cierto talento natural para el sarcasmo; podía burlarse a pesar de que no pudo
reír; pero siempre hay en su sátira algo incorpóreo y enclenque, sencillamente porque no estaba
respaldada por ningún fondo de moral corriente. Es en sí más grotesco que ninguna de sus expresiones.

Pero ciertamente, Nietzsche se mantendrá como exponente del fracaso total de la violencia abstracta. El
reblandecimiento cerebral que finalmente se apoderó de él, no fue un accidente físico. Si Nietzsche no
hubiera concluido en la imbecilidad, el nietzchismo habría concluido imbécil Pensando aisladamente y
con orgullo, se termina por ser un idiota. El hombre cuyo corazón no se ablande, acabará con los sesos
reblandecidos.

Esta última tentativa de evadirse del intelectualismo, concluye en el intelectualismo y por
consiguiente en la muerte. La evasión fracasó. La admiración frenética de la ilegalidad y la adoración
materialista de la ley, terminan en la misma nada. Nietzsche escala montañas vacilantes y finalmente
aparece en el Tibet. Se sienta al lado de Tolstoy en el país del vacío. Ambos son inválidos, uno porque no
puede retener nada y otro porque no puede perder nada.

La voluntad dé Tolstoy se congela por la intuición budista de que todas las acciones especiales son
malas. Pero la voluntad de Nietzsche igualmente se congela por su teoría de que todas las acciones
especiales son buenas, porque si todas las acciones especiales son buenas, ninguna de ellas es especial.

Hacen alto en la encrucijada, y uno odia todos los caminos y al otro le gustan todos. El resultado es
bueno; algunos no son difíciles de prever: hacen alto en la encrucijada.

Aquí termino (gracias a Dios) el primer y más árido asunto de este libro; la revisión sumaria del
pensamiento reciente. Después de esto, comienzo a describir otro aspecto de la vida que tal vez no
interesa al lector, pero que a mí por lo menos me interesa. Con ese objeto he hojeado una pila de libros
modernos que tengo ante mí al terminar esta página; una pila de ingenuidades, una pila de fruslerías. Por
mi presente desprendimiento accidental, puedo ver el inevitable choque de las filosofías .de Shopenhauer
y Tolstoy, de Nietzsche y Shaw, tan claramente como se puede ver desde, un globo, un choque de trenes.

Todos están encaminados a la vaciedad del hospicio.

Porque la locura puede definirse como uso de la actividad mental, hasta llegar a la impotencia
mental; y todos ellos, casi han llegado. Aquel que piense que está hecho de vidrio, piensa en pro de la
destrucción del pensamiento; porque el vidrio no puede pensar. Así también el que no quiere rechazar
nada, quiere en pro de la destrucción de la voluntad; porque voluntad no es sólo poder elegir algo, sino
rechazar casi todo. Y así que vuelvo y revuelvo sobre los inteligentes, hermosos, cansadores e inútiles
libros modernos; el título de uno de ellos detiene mi mirada. Se llama \emph{Juana de Arco} de Anatole France.

Solamente lo he hojeado, pero una mirada bastó para recordarme la \emph{Vida de Jesús}, de Renán. Sigue el
mismo método que el reverente escéptico. Desacredita los relatos sobrenaturales que tienen algún
fundamento, simplemente contando historias naturales que no tienen fundamento alguno. Porque no
podemos creer en lo que hizo un santo; debemos pretender que sabemos exactamente lo que sintió. Pero
no menciono a ninguno de ambos libros con objeto de criticarlo, sino porque a causa de la accidental
combinación de los nombres, recordé dos sorprendentes ejemplos de sensatez que hacen desaparecer
todos los libros que tenía ante mí. Juana de Arco no se turbó en la encrucijada, ni rechazando todas las
sendas como Tolstoy ni aceptándolas todas como Nietzsche.

Eligió un camino y lo recorrió como reguero de pólvora. No obstante, cuando vine a pensar en ella,
vi que Juana poseía todo lo que fue verdad en Tolstoy y en Nietzsche; aun todo lo que en ambos fue
tolerable. Pensé en todo lo que es noble en Tolstoy; el placer de las cosas sencillas, especialmente de la
piedad sencilla, la deferencia para el pobre, la dignidad de las espaldas dobladas. Juana de Arco, tuvo
todo eso, más este gran agregado; que sobrellevó la pobreza tan bien como la había admirado, en tanto
que Tolstoy fue un aristócrata típico tratando de hallar su secreto. Y luego pensé en todo lo que había de
valiente, y de arrogante y de patético en el pobre Nietzsche, y su rebelión contra la vaciedad y la timidez
de nuestro tiempo. Pensé en su grito de alarma por el estático equilibrio del peligro, su ansiedad por la
disparada de los grandes caballos, su grito a las armas. Bien, Juana de Arco tuvo todo eso y, otra vez, con
esta diferencia; que no alabó la lucha, pero luchó. Sabemos que no temía a un ejército, mientras que
Nietzsche, por todo lo que sabemos, pudo tener miedo de una vaca. Tolstoy solamente alabó al
campesino; ella, fue campesina. Nietzsche alabó al guerrero; ella fue guerrero. Ella, los derrota a ambos
en sus propios ideales antagónicos; fue más dulce que el uno y más violenta que el otro. No obstante, fue
una persona perfectamente práctica que hizo algo, en tanto que ellos son feroces especuladores que no
hicieron nada. Era imposible que no cruzara por mi mente el pensamiento de que ella y su fe, tenían quizá
un secreto de unidad y utilidad moral, que se nos ha perdido. Y con este pensamiento vino otro más vasto
y la figura colosal de su Señor, cruzó por el teatro de mis reflexiones. La misma dificultad moderna que
ha sombreado el sujeto-materia de Anatole France, ha sombreado también el de Ernesto Renán. Renán
también aísla a la piedad del vigor, en su héroe. Renán llega a representar la justa ira contra Jerusalén,
como si fuera un mero quebranto nervioso luego de las idílicas expectativas de Galilea. ¡Como si hubiera
contradicción entre amar a la humanidad y odiar lo inhumano! Los altruistas con débiles voces denuncian
egoísta a Cristo. Los egoístas (con voces más débiles aún), lo denuncian altruista. En la atmósfera actual,
tales cavilaciones resultan bastante comprensibles. El amor del héroe es más terrible que el odio del
tirano. El odio del héroe es más generoso que el amor del filántropo. Existe una heroica y magnífica
sensatez de la cual los modernos sólo pueden recoger fragmentos. Existe un gigante, del cual sólo
podemos ver los brazos y las piernas moviéndose en torno a nosotros. Han desgarrado el alma de Cristo
en girones tontos de altruismo y dé egoísmo, y siguen igualmente des-concertados por su magnificencia
insana y por su insana mansedumbre. Se han repartido sus vestiduras y sobre su túnica echaron suerte; a
pesar de que su túnica carecía de costuras y era toda una desde arriba hasta abajo.
\finalCapituloOrnamento
\printendnotes
