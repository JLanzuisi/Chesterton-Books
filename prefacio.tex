\chapter{Prefacio}%
\label{cha:Prefacio}

\textsc{Por extraña casualidad}, a la misma hora en que, en su vivienda campesina de Beaconsfield, fallecía
Gilbert Keith Chesterton, anunciaba George Bernard Shaw, en Newcastle, que no hablaría más en
público.

Con estos mosqueteros, que tantas veces midieron sus armas dialécticas, el espectáculo de la
refriega ideológica perdió en Inglaterra sus dos más diestros, tenaces y fantásticos combatientes.

Chesterton y Shaw nacieron tal para cual. Dotados del mismo vigor polémico e idéntico afán
proselitista, iguales en ingenio, no existía bajo el sol una sola cuestión frente a la cual sus opiniones no se
encontraran en diametral oposición.

La oposición de sus opiniones encendió y mantuvo encandilada, sin un momento de desmayo,
durante dos generaciones, la más fragorosa batalla que engendró nunca la inventiva. Sus controversias
públicas eran como justas de la razón dirimidas con los fuegos artificiales de las paradojas, las sutilezas,
los retruécanos y las imágenes, donde el público olvidaba el objeto de la riña y se dejaba fascinar por el
deslumbrante espectáculo.

Shaw vencía en el arte de la dramatización de su causa, pero Chesterton le vencía en la sutileza que
infundía al argumento de la suya.

Como si quisiera compensarle de la monstruosa corpulencia que levantó sobre sus pies, el Creador
dotó el cerebro de Chesterton con el más ágil, elástico, fino entendimiento que puso en ninguno de
nuestros contemporáneos. Era tan gigantesco y pingüe que le llamaron \textquote{monumento andante de Londres},
y en una ocasión, durante un banquete en su honor, Bernard Shaw dijo a la hora de los discursos: \textquote{Tan
galante es nuestro agasajado, señores, que esta misma mañana les dejó su asiento en el tranvía a tres
señoras}.

Fantasía o imaginación no iban a la zaga de su figura en cuanto a exuberancia.
Aunque, superficialmente considerada, la obra de Chesterton aparece sólo como un intento
ingenioso de encontrar la verdad por procedimientos originales en los que el ingenio y la originalidad
semejan lo principal y la verdad lo secundario, en realidad ocurre todo lo contrario.

Chesterton vivió perpetuamente desasosegado por la idea de la verdad, y sus paradojas no eran sino
el doble lazo con que pretendía coger por los cuernos tan elusivo toro.

Su versatilidad estaba propulsada por el mismo desasosiego, el cual le llevaba del verso al artículo
de periódico; de éste al ensayo filosófico; del ensayo a la novela teológica, cuando no detectivesca, o al
discurso proselitista y a la controversia.

La búsqueda de la verdad le condujo al catolicismo en 1922 y, poco después, a la fundación del
movimiento distributista, en el que pretendía encarnar su ideología y al que, secundado por su fiel y
veterano escudero el escritor casticista Hilaire Belloc\footnote{Belloc (1870\enraya1953) fue un prolífico escritor franco-británico. Su fuerte fé católica influencia todo su trabajo. \textquote{Todo conflicto humano es, en última instancia, teológico.}}, dedicara la mayor parte de su astronómica energía
durante los diez últimos años.

Chesterton odiaba tanto al capitalismo como al comunismo, porque ambos destruyen igualmente la
propiedad privada individual, el ejercicio de los oficios manuales que, para él, constituyen la base de la
libertad y el desenvolvimiento espiritual del hombre.

En el imaginario \say{Reino distributivo} cada individuo es propietario de las herramientas con que
trabaja, ejerce su oficio individualmente y posee su vivienda. Para propulsar el triunfo del Estado
distributivo, que debe ser alcanzado por los medios constitucionales, \textquote{puesto que los ingleses aborrecen la
violencia}, Chesterton fundó un semanario, excelente y brillantemente escrito, titulado \say{G.\,K's Weekly},
es decir, \say{Semanario de Chesterton}, donde colaboraba una pléyade escogida de jóvenes intelectuales
católicos.

La concepción chestertoniana de la economía estaba íntimamente vinculada a la que tenía de la
libertad.
La libertad abstracta que la Reforma impuso sobre Europa es, según Chesterton, una maldición que
ha devorado la libertad concreta que se gozaba anteriormente en los pueblos de la Cristiandad:

\begin{quote}
La
libertad de la post-Reforma significa esto: cualquiera puede escribir un folleto, cualquiera puede dirigir un
partido, cualquiera puede imprimir un periódico, cualquiera puede fundar una secta. El resultado ha sido
que nadie posee su propia tienda o sus propias herramientas, que nadie puede beber un vaso de cerveza o
apostar a un caballo. Ahora yo les ruego a ustedes, con toda seriedad, que consideren la situación desde el
punto de vista del hombre del pueblo. ¿Cuántos seres humanos desean fundar sectas, escribir folletos o
dirigir partidos?
\end{quote}

Esta cita es un ejemplo característico del procedimiento con que Chesterton mezcla lo arbitraria y lo
lógico, el sentido común y lo absurdo para, después de fundirlos en el crisol de su imaginación, elevar el
resultado a teoría.

Tan natural como su extravagante figura física era en Chesterton la jovialidad intelectual, el gozo en
el puro juego de la inteligencia y la frase chispeante. Cualquier argumento podía ser convertido por él,
automáticamente, en un deslumbrador juego de prestidigitación.

Muchas de sus frases y de las incidencias de sus controversias se han convertido ya en leyenda que
el pueblo transmite de boca en boca. Un día debatía por la radio con un poeta defensor del verso libre,
quien le acusó de no entender la \say{nueva métrica}:

---Verso libre no es una nueva métrica, del mismo modo que dormir al
raso no es una nueva forma de arquitectura.

---Pero no podrá usted negar que es una revolución en la forma literaria.
	
---El verso libre es una revolución, respecto a la forma literaria, igual que el comer carne cruda es una
revolución respecto al arte de la cocina.

A la agudeza y mordacidad intelectual, que Ie hacían un enemigo temible, se unían en la inmensa
humanidad de Gilbert Keith una bondad y campechanía primitivas y populares que le convertían en el
más delicioso de los amigos. De su amistad privada disfrutaban muchos de aquellos con quienes
Chesterton cambiaba en público los más inflexibles mandobles: librepensadores, racionalistas,
protestantes, socialistas, eugenistas, y, especialmente, la encarnación misma de todos estos \say{ismos}, el
inescrutable, invencible, incorregible George Bernard Shaw.

Con Bernard Shaw y Lloyd George compartió Chesterton el privilegio único de que tanto en los
periódicos como en las conversaciones se le mencionara por las solas iniciales de su nombre. \textquote{¡Pobre G.\,K. Chesterton!}, se decía la gente al saludarse, en Londres, el día de su muerte.

Una de las mejores biografías que existe hoy de Bernard Shaw la escribió, en 1909, Chesterton.
Antes había escrito ya una de sus obras maestras, la biografía de poeta Browning.
Más tarde escribió las de Chaucer, Stevenson, Colbett, San Francisco de Asís y Santo Tomás de
Aquino. Dos meses antes de morir había terminado la suya propia.

Sus libros de poemas llenan casi una biblioteca. Uno de ellos se titula \emph{Bagatelas tremendas}. Las
dos novelas más famosas que escribió: \emph{El hombre que fue jueves} y \emph{El padre Brown}, están traducidas
al español, pero, en cambio, creo que no ha sido trasladado al castellano ninguno de sus últimos libros, ni
siquiera el epos de \emph{Lepanto}.

\emph{The Napoleon of Notting Hill} y \emph{A Club of Queer Trades} son novelas de la vida suburbana de
Londres, en las que revive el espíritu \say{pickwickiano}.\footnote{\emph{Los papeles póstumos del club pickwick} es la primero novela publicada por Charles Dickens en 1836, cuyo personaje principal es Samuel Pickwick.} Chesterton hace de los personajes de sus novelas
instrumentos en que emplear su ingenio y les obliga a proceder del modo más incongruente que jamás
procedieron los habitantes del mundo novelesco.

De entre las obras teóricas o filosóficas, aparte de \emph{Ortodoxia}, aquella en que la ideología del autor
adquiere más coherencia es la contenida en el tomo de ensayos sobre el tema \emph{Qué hay de malo en el
mundo}, donde arguye contra las concepciones eugenistas,\footnote{Y no solo contra ellas: el capitalismo y, según Chesterton sus cómplices, los socialistas reciben pocos halagos de su parte. El capítulo final de dicho libro es quizás el mejor llamado a la revolución nunca escrito, partiendo de los cabellos rojos (como el fuego) de una niña pequeña.} las cuales asumen que la suerte de la vida está
determinada por el nacimiento, y hace la más impresionante descripción del concepto cristiano de la vida
que se haya escrito en este siglo.

Aunque sostuvo siempre la opinión de que el viajar contrae la inteligencia y apoca la fantasía, visitó
Italia, Irlanda y América y escribió un libro sobre las impresiones recibidas en cada uno de dichos países.

Al revés que Bernard Shaw y Wells, las otras dos grandes figuras de las letras inglesas de su
tiempo, Chesterton no sufrió privaciones en su juventud, sino que disfrutó de la más esmerada educación
que en aquella época podía recibir un hijo de burgueses ricos.

A pesar de que era dieciocho años más joven que Bernard Shaw, sus obras comenzaron a ser
conocidas al mismo tiempo que las de éste. Chesterton no desempeñó nunca, en realidad, otra ocupación
que la de escritor, a la que se dedicó por entero desde los veinte años, después de haber abandonado el
aprendizaje de dibujante. Por entonces consistía su cultura, fundamentalmente, en un profundo
conocimiento de la Biblia que le había infundido el padre, propietario de un importante negocio de
alquileres. Por las venas de la madre corría sangre francesa.

Tuvo un solo hermano, Cecil, que se dedicó también al periodismo y había logrado gran renombre
cuando, poco después de la guerra, vino a sorprenderle la muerte.

A los veinticinco años se casó y de su matrimonio no le quedó ningún hijo a la viuda.
Su vida toda fue una portentosa exhibición de atletismo intelectual y de entusiasmo espiritual.

\begin{flushright}\small
	\addfontfeature{LetterSpace=8} AUGUSTO ASSÍA.
\end{flushright}
\finalCapituloOrnamento
\printendnotes
