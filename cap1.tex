\chapter{Introducción en defensa de todo lo demás}%
\label{cha:Introducción en defensa de todo lo demás}

\textsc{La única justificación posible} para este libro, consiste en ser la respuesta a un desafío. Hasta un mal
tirador se dignifica aceptando un duelo.

Cuando hace algún tiempo publiqué una serie de apresurados; pero sinceros ensayos bajo el título
de \say{Heréjes}, algunos críticos por cuyas inteligencias siento caluroso respeto (puedo mencionar
especialmente al señor G.\,S. Street,\footnote{George Slythe Street (1867\enraya) fue un novelista y periodista británico. Trabajaba para el \say{National Observer}, un periódico conservador apologista del imperialismo de los años de preguerra.}) dijeron que estaba muy bien de mi parte sugerir a todos que probaran
su teoría cósmica, pero que yo había evitado diligentemente confirmar mis consejos con el ejemplo. \textquote{Voy a comenzar a preocuparme por mi filosofía,} dijo el señor Street, \textquote{cuando el señor Chesterton nos haya expuesto la suya}. Tal vez fue imprudente hacer tal indicación a una persona demasiado dispuesta a
escribir libros por la provocación más leve.\footnote{Chesterton no bromea: llegó a escribir mas de 170 libros, entre ellos varias recopilaciones de cuentos y artículos.} Pero después de todo, aunque el señor Street haya inspirado y
provocado la creación de este libro, no tiene ninguna necesidad de leerlo.

Si lo lee, verá que en forma personal, en sus páginas he intentado dar testimonio de la filosofía en la
cual he venido a creer, valiéndome de un conjunto de imágenes mentales más que de una serie de
deducciones. No voy a llamarla \say{mi filosofía}, porque yo No la hice. Dios y la Humanidad la hicieron; y
ella me hizo a mí.

Con frecuencia he sentido deseos de escribir una novela sobre un \say{yachtman} inglés que erró
levemente su ruta y descubrió Inglaterra convencido de haber descubierto una nueva isla en los mares del
Sur. No obstante, siempre me encontré demasiado perezoso o demasiado ocupado para escribir sobre ese
refinado tema. Por consiguiente puedo postergar una vez más mi deseo, ahora por fines de ilustración
filosófica.

Probablemente existirá la impresión general de que se sintió muy tonto el hombre que llegó a tierra
(armado hasta los dientes y hablando por señas) para plantar la bandera inglesa sobre aquél templo
bárbaro que resultó ser el Pabellón de Brighton. No me concierne a mí negar que parecía tonto. Pero si
ustedes se imaginan que se sintió tonto, por lo menos que la sensación de tontera fue su única y
dominante emoción, significa que no han estudiado con minuciosidad suficiente, la rica naturaleza
romántica del héroe de este cuento. Su error fue en verdad un error muy envidiable. Y él lo sabía, si era el
hombre que yo imagino.

¿Qué podría ser más agradable que sentir, simultáneamente y en pocos minutos, todas las
fascinadoras angustias del partir, combinadas con toda la seguridad humana de volver a casa? ¿Qué mejor
que gozar con la diversión de descubrir África, sin tener la desagradable necesidad de trasladarse a ese
continente? ¿Qué podría ser más agradable que felicitarse por descubrir Nueva Gales del Sur y
comprender luego, con lágrimas de alegría, que en realidad no era más que la vieja Gales del Sur?
Este, al menos a mi parecer, es el problema principal de los filósofos y en cierta forma, el principal
problema de este libro.

¿Cómo es posible que el mundo nos asombre y al mismo tiempo nos hallemos en él como en
nuestra casa?
¿Cómo puede este pueblo cósmico, con sus monstruos y lámparas antiguas, cómo este mundo puede
hacernos sentir simultáneamente, la fascinación de un pueblo exótico y el confort y el honor de ser
nuestro propio pueblo?
Demostrar que una creencia o una filosofía es verdadera desde todo punto de vista, sería empresa
demasiado grande aún para un libro más vasto que éste; es necesario atenerse a una sola línea de
argumentación; y esa es la táctica que me propongo observar.

Quiero dejar expuesta mi fe, como llenando esa doble necesidad espiritual: la necesidad de aliar lo
familiar con lo extraño, afiliación que con acierto el cristianismo llama \emph{romance}. Porque la misma
palabra \emph{romance}, tiene en sí el misterio y el primitivo significado de \emph{Roma}.

Cualquiera que se disponga a discutir algo, debe empezar siempre, especificando qué es lo que no
discute. Antes de determinar qué se propone probar, debería determinarse qué es lo que no se propone
probar.

Lo que no intento probar, lo que me propongo dejar como lugar común a mí y a la mayoría de los
lectores, es esta inclinación a una vida activa e imaginativa, pintoresca y llena de poética curiosidad; a
una vida como la que el hombre occidental, por lo menos aparenta haber deseado siempre.

Si un hombre opina que la extinción es mejor que la existencia o que una vida vacía y monótona es
mejor que la variación y la aventura, ese hombre no es uno de los seres normales a quienes me dirijo. Si
un hombre no tiene preferencia por nada, nada puedo darle. Pero aproximadamente todas las personas que
he encontrado en esta sociedad occidental en que vivo, estarían de acuerdo con la idea general de que
necesitamos esta vida de novela práctica; la combinación de algo que es extraño y problemático con algo
que es familiar y seguro. Necesitamos eso para vislumbrar al mundo combinando una idea de asombro
con una idea de bienvenida. Necesitamos ser felices en este mundo de maravillas sin sentirnos en él ni
siquiera confortables. Es esta enseñanza concluyente de mi credo, lo que voy a contemplar en las
siguientes páginas.

Pero tengo una razón personal para mencionar al hombre en el yacht que descubrió Inglaterra.
Porque ese hombre soy yo. Yo descubrí Inglaterra.
No sé cómo podría evitar que este libro girara en tomo al \say{ego}; y para decir verdad no sé cómo
evitar que resulte árido y confuso.
Su aridez, sin embargo, me librará del reproche que más lamento, el reproche de ser irónico y
petulante.

El sofisma liviano, es lo que más desprecio y tal vez resulte un hecho saludable que se me acuse
precisamente de usar de él. No conozco nada más despreciable que una simple paradoja; que es una
simple e ingeniosa defensa de lo indefinible. Si fuera cierto (según se ha dicho) que el señor Bernard
Shaw vivía de paradojas, el señor Bernard Shaw sería un vulgar millonario, porque un hombre de su
actividad mental, puede inventar un sofisma cada seis minutos. Inventar un sofisma es tan fácil como
mentir; porque es mentir. Lo cierto, naturalmente, es que el señor Shaw se ha visto cruelmente trabado,
por el hecho de que no puede decir una mentira, a menos que piense decir una verdad.

Yo también me siento bajo la misma intolerable trabazón. Jamás en mi vida dije nada por la sola
razón de creer gracioso lo que decía; no obstante, es claro que he tenido la vulgar vanidad humana, de
hallarlo gracioso porque yo lo había dicho.

Narrar una entrevista con una gorgona, criatura que no existe, es una cosa. Y otra cosa es descubrir
que el rinoceronte existe y deleitarse luego en el hecho de que parece que no existiera.

Se busca la verdad, pero es posible que instintivamente se persigan las verdades más increíbles, y
ofrezco este libro, con los sentimientos profundos del corazón, a la buena gente que detesta lo que escribo
y lo mira (muy justamente a mi entender) como una pobre payasada o como ejemplar de broma de mal
gusto.

Porque si este libro es una broma, es una broma contra mí mismo. Soy el hombre que haciendo
derroche de audacia, descubrió lo que ya había sido descubierto.

Si hay una sombra de farsa en lo que sigue, yo, soy el objeto de esa farsa; porque este libro explica
cómo imaginé ser el primero en poner pie en Brighton y cómo descubrí luego, que en realidad era el
último.
Cuento mis fantásticas aventuras en busca de lo evidente.

Nadie podría hallar mi caso más ridículo de lo que lo pienso yo; ningún lector puede acusarme aquí
de intentar ridiculizarlo. Yo soy el ridículo de esta historia y nadie ha de rebelarse para arrojarme de mi
trono. Confieso abiertamente todas las ambiciones de fines del siglo \textsc{xix}. Yo, como otros solemnes
chiquilines, traté de anticiparme a la época. Como ellos, intenté adelantarme por diez minutos a la verdad,
y encontré que ella se me había adelantado unos 1800 años. Esforcé la voz gritando mis verdades con
una penosa exageración juvenil, y recibí el castigo más adecuado, porque yo conservé mis verdades, pero
descubrí luego que si bien mis verdades eran verdades, mis verdades no eran mías.

Me hallé en la ridícula situación de creer que me sostenía sólo: estando en realidad sostenido por
toda la cristiandad.
Posiblemente, (y el ciclo me perdone) traté de ser original; pero sólo llegué a inventar una copia
imperfecta, de las ya existentes tradiciones de la religión civilizada. El hombre del yacht creyó descubrir
Inglaterra; yo creí descubrir Europa.

Traté de encontrar para mi uso, una herejía propia, y cuando la perfeccionaba con los últimos
toques, descubrí que no era herejía, sino simple ortodoxia.

Es posible que alguien se divierta con el relato de este chasco feliz; es posible que un amigo o un
enemigo se entretenga leyendo cómo gradualmente aprendí la verdad de una leyenda falseada o de la
falsedad de alguna filosofía difundida, cosas que pude aprender en mi catecismo. Si alguna vez lo hubiera
estudiado.

Es posible que haya diversión, o que no la haya, en leer cómo encontré al fin, en mi club anarquista
o en un templo babilónico, lo que pude encontrar en la iglesia parroquial vecina.

Si alguien se entretiene enterándose cómo las flores del campo o las frases que se oyen en el
ómnibus, o los incidentes de los políticos, o las preocupaciones de los jóvenes, se unieron en un cierto
orden para producir una cierta convicción de ortodoxia cristiana, ese alguien posiblemente pueda leer este
libro.

Pero en todo cabe una razonable división del trabajo. Yo escribí el libro, pero nada en el mundo
podría inducirme a leerlo.

Agrego una advertencia esencialmente pedante. Estos ensayos se limitan a discutir el hecho actual,
de que en el eje central de la teología cristiana (suficientemente resumida en el Símbolo de los Apóstoles)
se halla el mejor punto de apoyo para una ética enérgica y consistente.

Mis ensayos no intentan discutir el interesante, pero diferente punto de cuál es la actual sede de
autoridad que proclama ese Credo.

Aquí, el término ortodoxia, significa \say{credo de los Apóstoles} según lo entienden los que se
llamaban cristianos hasta hace muy poco tiempo y según la conducta histórica, de los que sostuvieron tal
credo.

Por razones de espacio me he visto forzado a limitarme a lo que he extractado de ese Credo; no toco
el asunto, tan discutido por los cristianos modernos, del origen del cual nosotros lo obtuvimos.

Esto no es un tratado eclesiástico, sino una autobiografía un poco deshilada.
Pero si alguno quiere saber mi opinión sobre la actual sede de autoridad de tal creencia, el señor G.\,S. Street no tiene más que arrojarme un nuevo desafío, y gustoso le escribiré otro libro.
\finalCapituloOrnamento
\printendnotes
